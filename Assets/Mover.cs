﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Mover : MonoBehaviour {

	[SerializeField] Vector3 movementVector = new Vector3(10f,10f,10f);
	[SerializeField] float period = 2f;


	[Range(0,1)]
	[SerializeField]
	float movementFactor;
	// Use this for initialization
	
	Vector3 startPos;
	
	void Start () {
		startPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		float cycles = Time.time / period;
		
		const float tau = Mathf.PI * 2;
		
		float raw =  Mathf.Sin(cycles * tau);
		
		movementFactor = raw / 2f + 0.5f;
		Vector3 offset = movementVector * movementFactor;
		transform.position = startPos + offset;
	}
}
