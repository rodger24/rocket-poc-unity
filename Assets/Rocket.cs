﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Rocket : MonoBehaviour {

	[SerializeField] float rcsThrust = 10f;
	[SerializeField] float mainThrust = 3f;


	Rigidbody rbody;
	// Use this for initialization
	void Start () {
		rbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		Thrust();
		Rotate();	
	}
	
	void LoadNextScence() {
		
		int sceneNum = SceneManager.GetActiveScene().buildIndex;
		SceneManager.LoadScene((sceneNum + 1) % 2);

	}

	void OnCollisionEnter(Collision collision) {
		switch (collision.gameObject.tag) {
			case "Freindly":
				print ("Friendly");
				break;
			case "Finish":
				Invoke("LoadNextScence",1f);
				break;
			default:
				 print ("not friendly");
				 break;
		}
		
		
	}

	private void Rotate() {
	
		float rotationSpeed = rcsThrust * Time.deltaTime;
		if (Input.GetKey(KeyCode.A))
		{
			transform.Rotate(Vector3.forward * rotationSpeed);
		}

		if (Input.GetKey(KeyCode.D))
		{
			transform.Rotate(-Vector3.forward * rotationSpeed);
		}
	}

	private void Thrust() {
		if (Input.GetKey(KeyCode.Space))
		{
			rbody.AddRelativeForce(Vector3.up * mainThrust);
		}
	
	}
}
